/**
 * 功能说明:
 * 功能作者:
 * 创建日期:
 * 版权归属:每特教育|蚂蚁课堂所有 www.abing.com
 */
package com.abing.api.fallback;

import com.abing.api.entity.UserEntity;
import com.abing.api.feign.MemberServiceFeigin;
import com.abing.api.service.IMemberService;
import com.abing.base.BaseApiService;
import com.abing.base.ResponseBase;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

@Component
public class MemberServiceFallback extends BaseApiService implements MemberServiceFeigin {
    @Override
    public UserEntity getMember(String name) {
        return null;
    }

    @Override
    public ResponseBase getUserInfo() {
        return setResultError("服务器忙，请稍后重试！以类方式写服务器降级！");
    }

}
